#include "Level02.h"
#include "BioEnemyShip.h"

//Test

void Level02::LoadContent(ResourceManager* pResourceManager)
{
	// Setup enemy ships
	Texture* pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");

	const int COUNT = 40;

	double xPositions[COUNT] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55,
		0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5,
		0.85, 0.8, 0.75, 0.7, 0.65, 0.6, 0.55, 0.5,
		0.35, 0.45, 0.55
	};

	double delays[COUNT] =
	{
		1.0, 1.25, 1.25, // Adjust the spawn time for the bio enemies
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3,
		2.5, 0.35, 0.35, 0.35, 0.35, 0.35, 0.35, 0.35,
		2.5, 0.35, 0.35, 0.35, 0.35, 0.35, 0.35, 0.35,
		1.5, 0.15, 0.15
	};

	float delay = 3.0; // start delay
	Vector2 position;

	for (int i = 0; i < COUNT; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		BioEnemyShip* pEnemy = new BioEnemyShip();
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);
	}

	Level::LoadContent(pResourceManager);
}