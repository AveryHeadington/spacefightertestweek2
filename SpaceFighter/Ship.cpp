
#include "Ship.h"


Ship::Ship() // I look at this as the ship settings and make it all work
{
	SetPosition(0, 0);
	SetCollisionRadius(10);

	//Sets the speed of your ship and Max hit points.
	//With current set up if you get hit you do not have any more hit points even if this says 3
	m_speed = 300;
	m_maxHitPoints = 3;
	m_isInvulnurable = false;

	Initialize();
}

//Updates position of the weapon blast 
void Ship::Update(const GameTime *pGameTime)
{
	m_weaponIt = m_weapons.begin();
	for (; m_weaponIt != m_weapons.end(); m_weaponIt++)
	{
		(*m_weaponIt)->Update(pGameTime);
	}

	GameObject::Update(pGameTime);
}
//Knows if it hits an enemy ship
void Ship::Hit(const float damage)
{
	if (!m_isInvulnurable)
	{
		m_hitPoints -= damage;

		if (m_hitPoints <= 3)
		{
			GameObject::Deactivate();
		}
	}
}

//Makes hitPoints = maxHitPoints...
void Ship::Initialize()
{
	m_hitPoints = m_maxHitPoints;
}

//Takes user input and will fire if they clock the fire button.
void Ship::FireWeapons(TriggerType type)
{
	m_weaponIt = m_weapons.begin();
	for (; m_weaponIt != m_weapons.end(); m_weaponIt++)
	{
		(*m_weaponIt)->Fire(type);
	}
}

//this takes the weapon and sets in on to the players ship so it knows where to fire from
void Ship::AttachWeapon(Weapon *pWeapon, Vector2 position)
{
	pWeapon->SetGameObject(this);
	pWeapon->SetOffset(position);
	m_weapons.push_back(pWeapon);
}